import Hls from "hls.js";

const videoUrls = [
  "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8",
  "https://multiplatform-f.akamaihd.net/i/multi/april11/sintel/sintel-hd_,512x288_450_b,640x360_700_b,768x432_1000_b,1024x576_1400_m,.mp4.csmil/master.m3u8",
  "https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8",
  "https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8",
  "http://d3rlna7iyyu8wu.cloudfront.net/skip_armstrong/skip_armstrong_stereo_subs.m3u8",
  "http://amssamples.streaming.mediaservices.windows.net/91492735-c523-432b-ba01-faba6c2206a2/AzureMediaServicesPromo.ism/manifest(format=m3u8-aapl)",
  "https://pl.streamingvideoprovider.com/mp3-playlist/playlist.m3u8",
];
const videoNode = document.getElementById("video");
const imageNode = document.getElementById("image");
const inputNode = document.getElementById("input");
const selectNode = document.getElementById("select");
const messageNode = document.getElementById("message");

inputNode.addEventListener("change", loadVideo, false);

for (let url of videoUrls) {
  const optionNode = document.createElement("option");
  optionNode.value = url;
  optionNode.textContent = url;
  selectNode.appendChild(optionNode);
}

selectNode.addEventListener("change", () => {
  inputNode.value = selectNode.value;
  loadVideo();
});

let hls;
function loadVideo() {
  if (hls) {
    hls.destroy();
  }
  messageNode.innerHTML = "";
  videoNode.style = "display: none";
  videoNode.src = "";
  imageNode.style = "display: none";
  imageNode.src = "";

  const videoSrc = inputNode.value;
  fetch(videoSrc).then((response) => {
    response.body.cancel();
    const type = response.headers.get("content-type");
    if (type.indexOf("ffmpeg") > -1) {
      imageNode.style = "display: block";
      imageNode.src = videoSrc;
      return;
    }
    hls = new Hls();
    hls.on(Hls.Events.ERROR, function (event, data) {
      const errorType = data.type;
      const errorDetails = data.details;
      const errorFatal = data.fatal;
      messageNode.innerHTML = `<strong>Error type:</strong> ${errorType}<br><strong>Detail</strong>: ${errorDetails}<br><strong>Fatal</strong>: ${errorFatal}`;
    });
    videoNode.style = "display: block";
    if (Hls.isSupported()) {
      hls.loadSource(videoSrc);
      hls.attachMedia(videoNode);
    } else if (videoNode.canPlayType("application/vnd.apple.mpegurl")) {
      videoNode.src = videoSrc;
    }
  });
}
